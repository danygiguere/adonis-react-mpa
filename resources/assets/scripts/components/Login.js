import React, { Component } from 'react'
import ReactDOM from 'react-dom'

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: 'johndoe@test.com',
      password: 'secret',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  handleSubmit(event) {
    event.preventDefault();
    axios.post('/login', this.state).then(response => {
      window.location.replace(window.location.protocol + "//" + window.location.host + "/products" );
    }).catch(error => {
        console.log('error', error);
    })
  }

  render() {
    return (
      <section className="light-background">
        <div className="container">
          <h2 className="title text-center">Login</h2>
          <div className="center-hz">
            <form onSubmit={this.handleSubmit} id="login-form">
              <div>
                <label htmlFor="">Email</label>
                <input type='email' name='email' value={this.state.email} onChange={this.handleChange}/>
              </div>
              <div className="margin-top">
                <label htmlFor="">Password</label>
                <input type='password' name='password' value={this.state.password} onChange={this.handleChange}/>
              </div>
              <div className="margin-top right">
                <button type="submit">Login</button>
              </div>
              <div className="margin-top right">
                <a href="/register" className="">Don't have an account ? Register here</a>
              </div>
            </form>
          </div>
        </div>
      </section>
    )
  }
}

if (document.getElementById('login')) {
  ReactDOM.render(<Login />, document.getElementById('login'))
}
