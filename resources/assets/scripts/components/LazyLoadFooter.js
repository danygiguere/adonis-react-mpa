import React, { Component, Suspense } from 'react'
import ReactDOM from 'react-dom'
const Footer = React.lazy(() => import('./Footer.js'));

export default class LazyLoadFooter extends Component {
    render() {
        return (
          <Suspense fallback={<div>Loading...</div>}>
            <Footer />
          </Suspense>
        )
      }
}

if (document.getElementById('footer')) {
    ReactDOM.render(<LazyLoadFooter />, document.getElementById('footer'));
}