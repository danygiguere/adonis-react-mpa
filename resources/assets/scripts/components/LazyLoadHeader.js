import React, { Component, Suspense } from 'react'
import ReactDOM from 'react-dom'
const Header = React.lazy(() => import('./Header.js'));

export default class LazyLoadHeader extends Component {
    render() {
        return (
          <Suspense fallback={<div>Loading...</div>}>
            <Header />
          </Suspense>
        )
      }
}

if (document.getElementById('header')) {
    ReactDOM.render(<LazyLoadHeader />, document.getElementById('header'));
}