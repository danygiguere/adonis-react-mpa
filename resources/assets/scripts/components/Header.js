import React, { Component } from 'react';

export default class Header extends Component {
    render() {
        return (
            <header>
                <nav>
                    <div className="menu">
                        <div className="logo">
                            <a href="/"><img src="/images/product.png" alt=""></img></a>
                        </div>
                        <div className="links">
                            <ul className="">
                                <li><a href="/products">Products</a></li>
                                <li className="dropdown"><span>Second link</span>
                                    <ul>
                                        <li><a href="#!">Second link (A)</a></li>
                                        <li><a href="#!">Second link (B)</a></li>
                                        <li><a href="#!">Second link (C)</a></li>
                                        <li><a href="#!">Second link (E)</a></li>
                                    </ul>
                                </li>
                                <li><a href="/login">Login</a></li>
                                <li><a href="/register">Register</a></li>
                            </ul>
                        </div>
                        <div className="menu-button">
                            <div className="bars">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>
        );
    }
}