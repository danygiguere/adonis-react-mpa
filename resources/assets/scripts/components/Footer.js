import React, { Component } from 'react';

export default class Footer extends Component {
    render() {
        return (
            <footer className="dark-background">
                <div className="container">
                    <p>&copy; Info</p>
                </div>
            </footer>
        );
    }
}