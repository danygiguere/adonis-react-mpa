const mix = require('laravel-mix')

// NOTE: Don't remove this, Because it's the default public folder path on AdonisJs
mix.setPublicPath('public')
mix.disableNotifications();

mix.webpackConfig({
  output: {
      // create chunked components for lazyloading
      publicPath: '/',
      chunkFilename: 'scripts/components/[name].js',
  },
}).version();

// Add your assets here
mix
  .js('resources/assets/scripts/app.js', 'scripts')
  .sass('resources/assets/styles/app.scss', 'styles')
